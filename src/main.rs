use bevy::{
    diagnostic::{FrameTimeDiagnosticsPlugin, LogDiagnosticsPlugin},
    prelude::*,
    winit::WinitSettings,
};

#[cfg(feature = "filesystem_watcher")]
use bevy::{asset::ChangeWatcher, utils::Duration};

mod game;
mod menu;
mod splash;
mod utils;

const TEXT_COLOR: Color = Color::WHITE;

fn startup_system(mut commands: Commands) {
    commands
        // Insert as resource the initial value for the settings resources
        .insert_resource(DisplayQuality::Medium);
    commands.insert_resource(Volume(7))
}

fn print_settings_when_s_pressed(
    input: Res<Input<KeyCode>>,
    display_quality: Res<DisplayQuality>,
    volume: Res<Volume>,
) {
    if input.just_pressed(KeyCode::S) {
        info!(
            "display quality : {:?}\nvolume: {:?}",
            display_quality, volume
        );
    }
}

// One of the two settings that can be set through the menu. It will be a resource in the app
#[derive(Resource, Debug, Component, PartialEq, Eq, Clone, Copy)]
enum DisplayQuality {
    Low,
    Medium,
    High,
}

// One of the two settings that can be set through the menu. It will be a resource in the app
#[derive(Resource, Debug, Component, PartialEq, Eq, Clone, Copy)]
struct Volume(u32);

// Enum that will be used as a global state for the game
#[derive(Clone, Copy, Default, Eq, PartialEq, Debug, Hash, States)]
pub enum AppState {
    #[default]
    Splash,
    MainMenu,
    GameLoad,
    InGame,
    InGameMenu,
}

fn main() {
    App::new()
        .add_plugins((
            DefaultPlugins.set(AssetPlugin {
                #[cfg(feature = "filesystem_watcher")]
                watch_for_changes: ChangeWatcher::with_delay(Duration::from_secs(3)),
                ..Default::default()
            }),
            LogDiagnosticsPlugin::default(),
            FrameTimeDiagnosticsPlugin,
        ))
        .add_state::<AppState>()
        .insert_resource(WinitSettings::desktop_app())
        .add_systems(Startup, (startup_system, setup))
        .add_systems(Update, print_settings_when_s_pressed)
        // Adds the plugins for each state
        .add_plugins((splash::SplashPlugin, menu::MenuPlugin, game::GamePlugin))
        .run();
}

fn setup(mut commands: Commands) {
    commands.spawn(Camera2dBundle::default());
}

#[test]
fn startup_system_sets_default_settings() {
    let mut app = App::new();

    fn check_init_values(volume: Res<Volume>, display_quality: Res<DisplayQuality>) {
        assert_eq!(volume.0, 7);
        assert_eq!(*display_quality, DisplayQuality::Medium);
    }

    app.add_systems(Startup, startup_system)
        .add_systems(Update, check_init_values);

    // Run systems
    app.update();

    // Check resulting changes
    assert_eq!(app.world.get_resource::<Volume>().unwrap().0, 7);
    assert_eq!(
        *app.world.get_resource::<DisplayQuality>().unwrap(),
        DisplayQuality::Medium
    );
}
